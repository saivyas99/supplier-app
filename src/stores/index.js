import ThemeStore from "./ThemeStore";
import TankStore from "./TankStore";
import SupplierStore from "./SupplierStore";
export default {
    ThemeStore:new ThemeStore(),
    TankStore:new TankStore(),
    SupplierStore:new SupplierStore()
};

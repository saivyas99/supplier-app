import { observable, action } from "mobx";
import {AsyncStorage,AppState} from "react-native";
import {PUBLIC_DOMAIN} from "../../constants";
import { Permissions, Notifications } from "expo";
export default class SupplierStore{
    @observable LoginResponse="";
    @observable authToken="";
    @observable isLoading=true;
    @observable internet_connection=true;
    @observable OrderHistory=[];
    @observable AccoutsList=[];
    @observable VehicleList=[];
    @action supplierLogin(post_json){
        var headers = {"Accept": "application/json", "Content-Type": "application/json","token":this.authToken};
        this.internet_connection=true;
        fetch(PUBLIC_DOMAIN+"api/mobile/endusers/registration/otp", {
            method: "POST",
            headers: headers,
            body:JSON.stringify(post_json)
        }).then((response) => {
            this.authToken=response.headers.get("token");
            AsyncStorage.setItem("authToken", String(this.authToken));
            response.json().then((responseJson) => {
                this.LoginResponse=responseJson;
                console.log("Login response", responseJson);
            });
        })
            .catch((error) =>{
                console.error("login error",error);
            });
    }
    @action supplierSignup(post_json,navigation){
        this.internet_connection=true;
        var headers = {"Accept": "application/json", "Content-Type": "application/json","token":this.authToken};
        fetch(PUBLIC_DOMAIN+"api/mobile/endusers/registration/otp/validation", {
            method: "POST",
            headers: headers,
            body:JSON.stringify(post_json)
        }).then((response) => {
            console.log("responsestatus", response.status);
            if(response.status==200){
                AsyncStorage.setItem("isLoggedIn", "true");
                navigation.navigate("SelectArea");
            }else{
                this.isVerfied=false;
                AsyncStorage.setItem("isLoggedIn", "false");
            }
            response.json().then((responseJson) => {
                this.VerifyResponse=responseJson;
                console.log("validate response", responseJson);
            });
        })
            .catch((error) =>{
                console.error("validate error",error);
            });
    }
    @action getOrderHistory(){
        var headers = {"Accept": "application/json", "Content-Type": "application/json","token":this.authToken};
        // Sample Order History
        this.OrderHistory=[{name:"Agastya Bandi",location:"Gachibowli",product:"Kenley - 35L",quantity:"5 Canes",orderid:"sup0909009",status:"P",image:"http://www.cinejosh.com/newsimg/newsmainimg/prabhas-new-deals-with-uv-and-gopi-krishna-films_b_2807161053.jpg"},{name:"Vivek Kara",location:"Madhapur",product:"Bilobang - 30L",quantity:"2 Canes",orderid:"sup0909009",status:"C",image:"https://pbs.twimg.com/profile_images/852874627181367297/BhCGvY6d_400x400.jpg"},{name:"Mesha Jaitley",location:"Ayyapa Society",product:"Aquafina - 35L",quantity:"2 Canes",orderid:"sup0909009",status:"D",image:"https://pbs.twimg.com/media/Dinp89IVQAAgD_N.jpg"},{name:"Agastya Bandi",location:"Gachibowli",product:"Kenley - 35L",quantity:"5 Canes",orderid:"sup0909009",status:"P",image:"http://www.cinejosh.com/newsimg/newsmainimg/prabhas-new-deals-with-uv-and-gopi-krishna-films_b_2807161053.jpg"},{name:"Vivek Kara",location:"Madhapur",product:"Bilobang - 30L",quantity:"2 Canes",orderid:"sup0909009",status:"C",image:"https://pbs.twimg.com/profile_images/852874627181367297/BhCGvY6d_400x400.jpg"},{name:"Mesha Jaitley",location:"Ayyapa Society",product:"Aquafina - 35L",quantity:"2 Canes",orderid:"sup0909009",status:"D",image:"https://pbs.twimg.com/media/Dinp89IVQAAgD_N.jpg"}];
        // ******
        // Uncomment this block and replace with api calls
        // fetch(PUBLIC_DOMAIN+"/api/", {
        //     method: "GET",
        //     headers: headers,
        // }).then((response) => response.json()).then((responseJson) => {
        //     this.OrderHistory=responseJson.statusResult;
        //     console.log("get order History response", responseJson);
        // }).catch((error) => {
        //     console.log("get order History error: "+ error);
        // });
        // ** End fetch block
    }
    @action getAccountsList(){
        var headers = {"Accept": "application/json", "Content-Type": "application/json","token":this.authToken};
        // Sample Order History
        this.AccoutsList=[{name:"Agastya Bandi",location:"Gachibowli",mobile:"+91988988909",amount:"1054",orderid:"sup0909009",status:"P",image:"http://www.cinejosh.com/newsimg/newsmainimg/prabhas-new-deals-with-uv-and-gopi-krishna-films_b_2807161053.jpg"},{name:"Vivek Kara",location:"Madhapur",mobile:"+91988988909",product:"Bilobang - 30L",amount:909,orderid:"sup0909009",status:"C",image:"https://pbs.twimg.com/profile_images/852874627181367297/BhCGvY6d_400x400.jpg"},{name:"Mesha Jaitley",location:"Ayyapa Society",mobile:"+91988988909",product:"Aquafina - 35L",amount:987,orderid:"sup0909009",status:"D",image:"https://pbs.twimg.com/media/Dinp89IVQAAgD_N.jpg"},{name:"Agastya Bandi",location:"Gachibowli",mobile:"+91988988909",product:"Kenley - 35L",amount:365,orderid:"sup0909009",status:"P",image:"http://www.cinejosh.com/newsimg/newsmainimg/prabhas-new-deals-with-uv-and-gopi-krishna-films_b_2807161053.jpg"},{name:"Vivek Kara",location:"Madhapur",mobile:"+91988988909",product:"Bilobang - 30L",amount:1878,orderid:"sup0909009",status:"C",image:"https://pbs.twimg.com/profile_images/852874627181367297/BhCGvY6d_400x400.jpg"},{name:"Mesha Jaitley",location:"Ayyapa Society",mobile:"+91988988909",product:"Aquafina - 35L",amount:8719,orderid:"sup0909009",status:"D",image:"https://pbs.twimg.com/media/Dinp89IVQAAgD_N.jpg"}];
        // ******
        // Uncomment this block and replace with api calls
        // fetch(PUBLIC_DOMAIN+"/api/", {
        //     method: "GET",
        //     headers: headers,
        // }).then((response) => response.json()).then((responseJson) => {
        //     this.OrderHistory=responseJson.statusResult;
        //     console.log("get order History response", responseJson);
        // }).catch((error) => {
        //     console.log("get order History error: "+ error);
        // });
        // ** End fetch block
    }
    @action getCatalogueList(){
        //Sample Catalogue List
        this.CatalogueList=[{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}},{id:1,avgRating:5,maxTimeToServeExpress:"30mins",serviceProviderServiceObj:{pricePerItem:"850"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}}];
        // ******
        // Uncomment this block and replace with api calls
        // this.internet_connection=true;
        // fetch(PUBLIC_DOMAIN+"api/", {
        //     method: "GET",
        //     headers: {},
        // }).then((response) => response.json()).then((responseJson) => {
        //     this.CatalogueList=responseJson.statusResult;
        //     this.isLoading=false;
        // }).catch((error) => {
        //     this.internet_connection=false;
        //     this.isLoading=false;
        //     console.log("fetch error: "+ error);
        // });
        this.isLoading=false;
    }
    @action getVehicleList(){
        //Sample Catalogue List
        this.VehicleList=[{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Tanker"}},{id:1,status:"running",vehicle_brand:"Ashok Leyland",serviceProviderServiceObj:{driverImage:"https://i.ytimg.com/vi/o8-QdJ0oV-Y/maxresdefault.jpg"},serviceObj:{itemDefinition:"10Ltrs"},serviceProviderObj:{serviceProviderIconPath:"https://images.click.in/classifieds/images/126/29496090_22_3_2018_20_18_57_415865686_x70r4j8w4f.jpg",serviceProvider:"Zugae Supplier"}}];
        // ******
        // Uncomment this block and replace with api calls
        // this.internet_connection=true;
        // fetch(PUBLIC_DOMAIN+"api/", {
        //     method: "GET",
        //     headers: {},
        // }).then((response) => response.json()).then((responseJson) => {
        //     this.CatalogueList=responseJson.statusResult;
        //     this.isLoading=false;
        // }).catch((error) => {
        //     this.internet_connection=false;
        //     this.isLoading=false;
        //     console.log("fetch error: "+ error);
        // });
        this.isLoading=false;
    }
    @action async registerForPushNotificationsAsync() {
        const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;
        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        // Get the token that uniquely identifies this device
        let token = await Notifications.getExpoPushTokenAsync();
        // POST the token to your backend server from where you can retrieve it to send push notifications.
        var headers = {"Accept": "application/json", "Content-Type": "application/json","token":this.authToken};
        let post_json={
            token:token,
            platform:Platform.OS
        };
        fetch(PUBLIC_DOMAIN+"Service to insert push token", {
            method: "POST",
            headers: headers,
            body:JSON.stringify(post_json)
        }).then((response) => response.json()).then((responseJson) => {
            console.log("Register Push Token response", responseJson);
        }).catch((error) => {
            console.log("Register Push Token error: "+ error);
        });
        AsyncStorage.setItem("device_registered",true);
    }
    @action changeValues(text,field){
        this[field]=text;
    }
}

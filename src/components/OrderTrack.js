import React, { Component } from "react";
import { AppRegistry, StyleSheet, Dimensions,Platform, Image, View, StatusBar, TouchableOpacity } from "react-native";
import {Button,Text,Item,Container,Icon,Card, CardItem, Thumbnail, Left, Body, Right} from "native-base";
import MapView,{ Marker, AnimatedRegion } from "react-native-maps";
import Polyline from "@mapbox/polyline";
import { observer, inject } from "mobx-react";
import Icon1 from "react-native-vector-icons/MaterialCommunityIcons";
const LATITUDE = 37.42140481;
const LONGITUDE = -122.22446999;
const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const markerIDs = ["Marker1", "Marker2"];
const timeout = 4000;
let animationTimeout;
@inject(["ThemeStore"])
@observer
export default class OrderTrack extends Component {
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:"Track Order",
      headerLeft:<Button transparent onPress={()=>navigation.goBack()} style={{width:100}}><Icon name="md-arrow-back"  style={{color:screenProps.iconColor.color,marginLeft:5,fontSize:20,padding:10}} /></Button>,
      headerRight:<View  style={{marginRight:5}} />,
  });
  constructor(props) {
      super(props);
      this.state = {
          latitude: LATITUDE,
          longitude: LONGITUDE,
          error: null,
          concat: null,
          coords:[],
          x: "false",
          cordLatitude:37.35240938,
          cordLongitude:-122.22346999,
          routeCoordinates: [],
          distanceTravelled: 0,
          prevLatLng: {},
          coordinate: new AnimatedRegion({
              latitude: LATITUDE,
              longitude: LONGITUDE
          })
      };
      this.mergeLot = this.mergeLot.bind(this);
  }
  componentDidMount() {
      // navigator.geolocation.getCurrentPosition(
      //     (position) => {
      //         this.setState({
      //             latitude: position.coords.latitude,
      //             longitude: position.coords.longitude,
      //             error: null,
      //         });
      //         this.mergeLot();
      //         console.log("position"+JSON.stringify(position));
      //     },
      //     (error) => this.setState({ error: error.message }),
      //     { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
      // );
      // this.watchID = navigator.geolocation.watchPosition(
      //     position => {
      //         const { coordinate, routeCoordinates, distanceTravelled } = this.state;
      //         const { latitude, longitude } = position.coords;
      //         const newCoordinate = {
      //             latitude,
      //             longitude
      //         };
      //         if (Platform.OS === "android") {
      //             if (this.marker) {
      //                 this.marker._component.animateMarkerToCoordinate(
      //                     newCoordinate,
      //                     500
      //                 );
      //             }
      //         } else {
      //             coordinate.timing(newCoordinate).start();
      //         }
      //         this.setState({
      //             latitude,
      //             longitude,
      //             routeCoordinates: routeCoordinates.concat([newCoordinate]),
      //             prevLatLng: newCoordinate
      //         });
      //     },
      //     error => console.log(error),
      //     { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      // );
      // animationTimeout = setTimeout(() => {
      //     this.focus1();
      // }, timeout);
  }
   getMapRegion = () => ({
       latitude: this.state.latitude,
       longitude: this.state.longitude,
       latitudeDelta: LATITUDE_DELTA,
       longitudeDelta: LONGITUDE_DELTA
   });
   mergeLot(){
       if (this.state.latitude != null && this.state.longitude!=null)
       {
           let concatLot = this.state.latitude +","+this.state.longitude;
           this.setState({
               concat: concatLot
           }, () => {
               this.getDirections(concatLot, "-6.270565,106.759550");
           });
       }
   }
   focus1() {
       animationTimeout = setTimeout(() => {
           this.focusMap([
               markerIDs[0],
               markerIDs[1],
           ], true);
       }, timeout);
   }
   focusMap(markers, animated) {
       console.log(`Markers received to populate map: ${markers}`);
       this.mapRef.fitToSuppliedMarkers(markers, animated);
   }
   async getDirections(startLoc, destinationLoc) {
       try {
           let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`);
           let respJson = await resp.json();
           let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
           let coords = points.map((point, index) => {
               return  {
                   latitude : point[0],
                   longitude : point[1]
               };
           });
           this.setState({coords: coords});
           this.setState({x: "true"});
           return coords;
       } catch(error) {
           console.log("masuk fungsi");
           this.setState({x: "error"});
           return error;
       }
   }
   render() {
       const ThemeStore=this.props.ThemeStore;
       return (
           <View style={{flex:1}}>
               <MapView style={styles.map} showUserLocation
                   followUserLocation
                   loadingEnabled
                   showUserLocation
                   followUserLocation
                   loadingEnabled
                   intialRegion={this.getMapRegion()}
                   ref={ref => { this.mapRef= ref; }}>
               </MapView>
               <Card style={{position:"absolute",bottom:0,left:0,width:"100%"}}>
                   <CardItem>
                       <Left>
                           <Body>
                               <Text>Order Id : <Text style={{color:ThemeStore.buttonColors}}>500050902</Text></Text>
                           </Body>
                       </Left>
                       <Right>
                           <Icon1  style={{fontSize:15,color:ThemeStore.buttonColors}} name="alert-outline" />
                       </Right>
                   </CardItem>
                   <CardItem cardBody>
                       <View style={{width:50,height:50,marginLeft:10,marginRight:30}}>
                           <Image  source={{uri:"http://www.cinejosh.com/newsimg/newsmainimg/prabhas-new-deals-with-uv-and-gopi-krishna-films_b_2807161053.jpg"}} style={{height:50,width:50,borderRadius:25, width: null,flex:1}}
                           />
                       </View>
                       <View>
                           <Text style={{color:ThemeStore.buttonColors}}>Zaid Wafid Sarraf</Text>
                           <Text style={{color:ThemeStore.textColor}}>Venkateshawara Water Suppliers</Text>
                           <Text style={{color:ThemeStore.textColor}}>Contact Driver : +91878389890</Text>
                       </View>
                   </CardItem>
                   <CardItem>
                       <Left>
                           <Button transparent>
                               <Text>Pending : 5 Orders</Text>
                           </Button>
                       </Left>
                       <Right>
                           <Text>Delivered : 19 Orders</Text>
                       </Right>
                   </CardItem>
               </Card>
           </View>
       );
   }
}
const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject
    }
});

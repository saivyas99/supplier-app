import React from "react";
import { Col, Row, Grid } from "react-native-easy-grid";
import { observer, inject } from "mobx-react";
import Fa from "react-native-vector-icons/FontAwesome";
import {Text, Image,TouchableHighlight,Alert,View,TouchableOpacity} from "react-native";
import { Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from "native-base";
import {PUBLIC_DOMAIN} from "../../constants";
@inject(["ThemeStore"])
@observer
export default class EachRowOrder extends React.Component {
    render(){
        const ThemeStore=this.props.ThemeStore;
        const navigation=this.props.navigation;
        const data=this.props.data;
        return(
            <TouchableOpacity onPress={()=>navigation.navigate("OrderTrack")}>
                <Card  key={data.orderid}>
                    <CardItem style={{flex:1,flexDirection:"row"}}>
                        <View style={{width:50,height:50,marginRight:10}}>
                            <Image  source={{uri:data.image}} style={{height:50,width:50,borderRadius:25, width: null,flex:1}}
                            />
                        </View>
                        <View style={{width:180}}>
                            <Text style={{color:ThemeStore.textColor}}>{data.name}{"\n"}{data.location}</Text>
                            <Text style={{color:ThemeStore.textColor}}>{"\n"}{data.product}{"    "}{data.quantity}</Text>
                        </View>
                        <View style={{width:50,position:"absolute",right:10}}>
                            {data.status=="D"?<View style={{backgroundColor:"grey",justifyContent:"center",alignItems:"center",borderRadius:20}}><Text style={{fontSize:40,color:"#fff"}}>D</Text></View>:data.status=="P"?<View style={{backgroundColor:"red",justifyContent:"center",alignItems:"center",borderRadius:20}}><Text style={{fontSize:40,color:"white"}}>P</Text></View>:<View style={{backgroundColor:"green",justifyContent:"center",alignItems:"center",borderRadius:20}}><Text style={{fontSize:40,color:"#fff"}}>C</Text></View>
                            }
                        </View>
                    </CardItem>
                    <CardItem>
                        <Body>
                        </Body>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        );
    }
}

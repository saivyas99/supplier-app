import React from "react";
import { observer, inject } from "mobx-react";
import {View,Image,TextInput,ScrollView} from "react-native";
import {Button,Text,Item,Input,Icon,Picker,Tab, Tabs,Segment,Label,Textarea} from "native-base";
import {APP_NAME} from "../../constants";
import PhoneInput from "react-native-phone-input";
import { NativeModules } from "react-native";
import Icon1 from "react-native-vector-icons/MaterialCommunityIcons";
import CatalogueList from "./CatalogueList.js";
import { ImagePicker } from "expo";
@inject(["ThemeStore"],["SupplierStore"])
@observer
class AddTicket extends React.Component{
  state = {
      vehicle_model:"",
      brand:"",
      reg_number:"",
      capacity:"",
      assign_driver:"",
      status:""
  };
  render(){
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <ScrollView>
              <View style={{flex:1,flexDirection:"column",paddingLeft:10,paddingRight:10}}>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({vehicle_model:text})} placeholder="Subject " />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Textarea  rowSpan={5} style={{width:"100%"}} bordered   onChangeText={(text) => this.setState({brand:text})} placeholder="Message" />
                  </View>
                  <Button style={{backgroundColor:ThemeStore.buttonColors,width:"50%",alignSelf:"center"}} block style={{backgroundColor:this.state.disabledColor,width:"90%",marginLeft:"5%"}}>
                      <Text style={{color:ThemeStore.buttonTextColor}}>Submit</Text>
                  </Button>
              </View>
          </ScrollView>
      );
  }
}
@inject(["ThemeStore"],["SupplierStore"])
@observer
export default class Support extends React.Component{
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:"Oasiz Support",
      headerLeft:<Button transparent onPress={()=>navigation.goBack()} style={{width:100}}><Icon name="md-arrow-back"  style={{color:screenProps.iconColor.color,marginLeft:5,fontSize:20,padding:10}} /></Button>,
      headerRight:<View  style={{marginRight:5}} />,
  });
  constructor(props){
      super(props);
  }
  componentWillMount() {
  }
  componentDidMount(){
  }
  render() {
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <Tabs>
              <Tab heading="New Ticket">
                  <AddTicket navigation={navigation} />
              </Tab>
              <Tab heading="Ticket List">
                  <CatalogueList navigation={navigation} />
              </Tab>
          </Tabs>
      );
  }
}

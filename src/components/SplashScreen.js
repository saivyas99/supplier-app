import React from "react";
import { observer, inject } from "mobx-react";
import { StyleSheet, Text,View,ActivityIndicator,Image,AsyncStorage,Dimensions} from "react-native";
import { Container, Header, Content,Body,Title,Button } from "native-base";
import Fa from "react-native-vector-icons/FontAwesome";
import {LinesLoader, DoubleCircleLoader, CirclesRotationScaleLoader, EatBeanLoader} from "react-native-indicator";
import {LOGO_IMG} from "../../constants";
const {width,height}=Dimensions.get("window");
@inject(["ThemeStore"])
@observer
export default class SplashScreen extends React.Component {
    constructor(props){
        super(props);
        const ThemeStore=this.props.ThemeStore;
        const navigation=this.props.navigation;
        this.state={
            text1:"Chaning Theme",
            text2:"Please Wait.."
        };
    }
    componentWillReceiveProps() {
        const ThemeStore=this.props.ThemeStore;
        const navigation=this.props.navigation;
        if (ThemeStore && !navigation.state.params) {
            navigation.setParams({headerTextColor: ThemeStore.textColor,headerColor:ThemeStore.headerColor});
        }
    }
static navigationOptions = ({ navigation,screenProps }) => ({
    header:null,
    headerRight:<View  style={{marginRight:5}} />,
    headerLeft: <View  style={{marginLeft:5}} />,
});
componentWillMount(){
    const ThemeStore=this.props.ThemeStore;
    const navigation=this.props.navigation;
}
async componentDidMount(){
    const ThemeStore=this.props.ThemeStore;
    const navigation=this.props.navigation;
    setTimeout(function(){
        navigation.navigate("LoginScreen");
    }, 4000);
}
render() {
    const ThemeStore=this.props.ThemeStore;
    const navigation=this.props.navigation;
    return (
        <View style={{backgroundColor:ThemeStore.headerBackgroundColor,flex:1,flexDirection:"column",justifyContent:"center",alignItems:"center"}} >
            <Image source={LOGO_IMG} style={{width:width,height:height,backgroundColor:"transparent"}} />
            <Text>{"\n"}</Text>
            <DoubleCircleLoader  color={ThemeStore.buttonTextColor} />
        </View>
    );
}
}

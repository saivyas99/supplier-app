import React from "react";
import { observer, inject } from "mobx-react";
import {Text,WebView, View,ActivityIndicator,FlatList,ScrollView,Platform,Image,Segment} from "react-native";
import { Container, Header, Content,Body,Title,Button,Icon,Tab, Tabs } from "native-base";
import Fa from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import EachRowOrder from "./EachRowOrder";
import {NineCubesLoader, DoubleCircleLoader, TextLoader, CirclesRotationScaleLoader} from "react-native-indicator";
import {APP_NAME} from "../../constants";
import Swiper from "react-native-swiper";
@inject(["ThemeStore"],["SupplierStore"],["TankStore"])
@observer
export default class OrderHistory extends React.Component {
  static navigationOptions = () => ({
      title:APP_NAME ,
  });
  constructor(props){
      super(props);
  }
  componentWillMount(){
      const SupplierStore=this.props.SupplierStore;
      SupplierStore.getOrderHistory();
  }
  render() {
      const SupplierStore=this.props.SupplierStore;
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      let OrderHistory=
        <View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
            <DoubleCircleLoader  color={ThemeStore.buttonColors} />
        </View>;
      if(SupplierStore.OrderHistory.length==0){
          OrderHistory=<View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
              <Fa name="gift" style={{color:ThemeStore.buttonColors,fontSize:50}} />
              <Text style={{color:ThemeStore.buttonColors}}>No Orders till now!.</Text>
          </View>;
      }else{
          OrderHistory=
              <View style={{justifyContent: "center",  flex:1,  paddingTop: (Platform.OS === "iOS") ? 20 : 0}}>
                  <Swiper style={styles.wrapper} showsButtons>
                      <View style={styles.slide}>
                          <Text style={styles.text}>New Order</Text>
                          <Text style={styles.text}>125</Text>
                      </View>
                      <View style={styles.slide}>
                          <Text style={styles.text}>New Order</Text>
                          <Text style={styles.text}>125</Text>
                      </View>
                      <View style={styles.slide}>
                          <Text style={styles.text}>New Order</Text>
                          <Text style={styles.text}>125</Text>
                      </View>
                  </Swiper>
                  <Tabs>
                      <Tab heading="Normal Order">
                          <ScrollView>
                              <FlatList
                                  data={SupplierStore.OrderHistory}
                                  renderItem={({item,index}) => <EachRowOrder  navigation={navigation} data={item} key={item._id}/>}
                                  keyExtractor={(item, index) => index}
                                  stickyHeaderIndices={[0]}
                              />
                          </ScrollView>
                      </Tab>
                      <Tab heading="Express Order">
                          <ScrollView>
                              <FlatList
                                  data={SupplierStore.OrderHistory}
                                  renderItem={({item,index}) => <EachRowOrder  navigation={navigation} data={item} key={item._id}/>}
                                  keyExtractor={(item, index) => index}
                                  stickyHeaderIndices={[0]}
                              />
                          </ScrollView>
                      </Tab>
                  </Tabs>
              </View>;
      }
      return (
          <Container>
              <Content>
                  {OrderHistory}
              </Content>
          </Container>
      );
  }
}
var styles = {
    wrapper: {
        height:300
    },
    slide: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#4733ff"
    },
    text: {
        color: "#fff",
        fontSize: 30,
        fontWeight: "bold"
    }
};

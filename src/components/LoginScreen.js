import React from "react";
import { observer, inject } from "mobx-react";
import {View,Image,TextInput,TouchableHighlight} from "react-native";
import {Button,Text,Item,Input} from "native-base";
import {APP_NAME} from "../../constants";
import PhoneInput from "react-native-phone-input";
import { NativeModules } from "react-native";
@inject(["ThemeStore"])
@observer
export default class LoginScreen extends React.Component{
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:APP_NAME,
  });
  constructor(props){
      super(props);
  }
  state = {
      isValid:false,
      mailid:"",
      password:""
  };
  componentWillMount() {
  }
  componentDidMount(){
  }
  VaidateLogin(){
      const navigation=this.props.navigation;
  }
  render() {
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <View style={{flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",padding:20}}>
              <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                  <Text style={{color:"#000"}}>Supplier Login</Text>
              </View>
              <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                  <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                      <Input  onChangeText={(text) => this.setState({mailid:text})} placeholder="e-Maild Id" />
                  </Item>
              </View>
              <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                  <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                      <Input  onChangeText={(text) => this.setState({password:text})} placeholder="Password" />
                  </Item>
              </View>
              <Button block style={{backgroundColor:ThemeStore.buttonColors,width:"80%"}} disabled={this.state.mailid=="" || this.state.password==""?true:false} onPress={() => this.VaidateLogin()} block style={{backgroundColor:this.state.disabledColor,width:"90%",marginLeft:"5%"}}>
                  <Text style={{color:ThemeStore.buttonTextColor}}>Login</Text>
              </Button>
              <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                  <TouchableHighlight  onPress={()=>navigation.navigate("SignupScreen")} ><Text style={{color:"blue"}}>{"\n"}Not a Member? Signup Now!</Text></TouchableHighlight>
              </View>
          </View>
      );
  }
}

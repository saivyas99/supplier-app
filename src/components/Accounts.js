import React from "react";
import { observer, inject } from "mobx-react";
import {Text,WebView, View,ActivityIndicator,FlatList,ScrollView,Platform,Image,Segment,TouchableOpacity} from "react-native";
import { Container, Header, Content, Card,CardItem,Body,Title,Button,Icon,Tab, Tabs,Left,Right } from "native-base";
import Fa from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import {NineCubesLoader, DoubleCircleLoader, TextLoader, CirclesRotationScaleLoader} from "react-native-indicator";
import {APP_NAME} from "../../constants";
@inject(["ThemeStore"])
@observer
export class EachRowAccount extends React.Component {
    render(){
        const ThemeStore=this.props.ThemeStore;
        const navigation=this.props.navigation;
        const data=this.props.data;
        return(
            <TouchableOpacity>
                <Card  key={data.orderid}>
                    <CardItem style={{flex:1,flexDirection:"row"}}>
                        <View style={{width:50,height:50,marginRight:10}}>
                            <Image  source={{uri:data.image}} style={{height:50,width:50,borderRadius:25, width: null,flex:1}}
                            />
                        </View>
                        <View style={{width:180}}>
                            <Text style={{color:ThemeStore.textColor}}>{data.name}{"\n"}{"Mobile :"}{data.mobile}</Text>
                            <Text style={{color:ThemeStore.textColor}}>{"\n"}{"Amount: "}{data.amount}</Text>
                        </View>
                        <View style={{width:50,position:"absolute",right:10}}>
                            {data.status=="D"?<View style={{backgroundColor:"grey",justifyContent:"center",alignItems:"center",borderRadius:20}}><Text style={{fontSize:40,color:"#fff"}}>D</Text></View>:data.status=="P"?<View style={{backgroundColor:"red",justifyContent:"center",alignItems:"center",borderRadius:20}}><Text style={{fontSize:40,color:"white"}}>P</Text></View>:<View style={{backgroundColor:"green",justifyContent:"center",alignItems:"center",borderRadius:20}}><Text style={{fontSize:40,color:"#fff"}}>C</Text></View>
                            }
                        </View>
                    </CardItem>
                    <CardItem>
                        <Body>
                        </Body>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        );
    }
}
@inject(["ThemeStore"],["SupplierStore"])
@observer
export default class Accounts extends React.Component {
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:"Accounts",
      headerLeft:<Button transparent onPress={()=>navigation.goBack()} style={{width:100}}><Icon name="md-arrow-back"  style={{color:screenProps.iconColor.color,marginLeft:5,fontSize:20,padding:10}} /></Button>,
      headerRight:<View  style={{marginRight:5}} />,
  });
  constructor(props){
      super(props);
  }
  componentWillMount(){
      const SupplierStore=this.props.SupplierStore;
      SupplierStore.getAccountsList();
  }
  render() {
      const SupplierStore=this.props.SupplierStore;
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      let OrderHistory=
        <View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
            <DoubleCircleLoader  color={ThemeStore.buttonColors} />
        </View>;
      if(SupplierStore.AccoutsList.length==0){
          OrderHistory=<View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
              <Fa name="gift" style={{color:ThemeStore.buttonColors,fontSize:50}} />
              <Text style={{color:ThemeStore.buttonColors}}>No Orders till now!.</Text>
          </View>;
      }else{
          OrderHistory=
              <View style={{justifyContent: "center",  flex:1,  paddingTop: (Platform.OS === "iOS") ? 20 : 0}}>
                  <ScrollView>
                      <FlatList
                          data={SupplierStore.AccoutsList}
                          renderItem={({item,index}) => <EachRowAccount  navigation={navigation} data={item} key={item._id}/>}
                          keyExtractor={(item, index) => index}
                          stickyHeaderIndices={[0]}
                      />
                  </ScrollView>
              </View>;
      }
      return (
          <Container>
              <Content>
                  <Card>
                      <CardItem>
                          <Left>
                              <Body>
                                  <Text style={{fontSize:18}}>15 May - 13 June</Text>
                                  <Text style={{fontSize:18}}>{"\n"}Cash Collected  : <Text style={{color:ThemeStore.buttonColors}}></Text></Text>
                                  <Text style={{fontSize:18}}>{"\n"}Online Payment  : <Text style={{color:ThemeStore.buttonColors}}></Text></Text>
                                  <Text style={{fontSize:18}}>{"\n"}Outstanding Amount  : <Text style={{color:ThemeStore.buttonColors}}></Text></Text>
                              </Body>
                          </Left>
                      </CardItem>
                  </Card>
                  {OrderHistory}
              </Content>
          </Container>
      );
  }
}
var styles = {
    wrapper: {
        height:300
    },
    slide: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#4733ff"
    },
    text: {
        color: "#fff",
        fontSize: 30,
        fontWeight: "bold"
    }
};

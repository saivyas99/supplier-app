import React from "react";
import { observer, inject } from "mobx-react";
import {Text,WebView, View,ActivityIndicator,FlatList,ScrollView,Platform} from "react-native";
import { Container, Header, Content,Body,Title,Button,Icon } from "native-base";
import Fa from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import EachRow from "./EachRow";
import {NineCubesLoader, DoubleCircleLoader, TextLoader, CirclesRotationScaleLoader} from "react-native-indicator";
import {APP_NAME} from "../../constants";
@inject(["ThemeStore"],["SupplierStore"])
@observer
export default class CatalogueList extends React.Component {
  static navigationOptions = () => ({
      title:APP_NAME ,
  });
  constructor(props){
      super(props);
  }
  componentWillMount(){
      const SupplierStore=this.props.SupplierStore;
      SupplierStore.getCatalogueList();
  }
  render() {
      const SupplierStore=this.props.SupplierStore;
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      let CatalogueList=
        <View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
            <DoubleCircleLoader  color={ThemeStore.buttonColors} />
        </View>;
      if(SupplierStore.CatalogueList.length==0){
          CatalogueList=<View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
              <Ionicon name="ios-water-outline" style={{color:ThemeStore.buttonColors,fontSize:50}} />
              <Text style={{color:ThemeStore.buttonColors}}>No Water Suppliers in your area!.</Text>
          </View>;
      }else{
          if(SupplierStore.isLoading){
              CatalogueList=<View style={{marginTop:200,flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                  <DoubleCircleLoader  color={ThemeStore.buttonColors} />
              </View>;
          }else {
              CatalogueList=
              <View style={{justifyContent: "center",  flex:1,  paddingTop: (Platform.OS === "iOS") ? 20 : 0}}>
                  <FlatList
                      data={SupplierStore.CatalogueList}
                      renderItem={({item,index}) => <EachRow  navigation={navigation} data={item} key={item._id}/>}
                      keyExtractor={(item, index) => index}
                      stickyHeaderIndices={[0]}
                  />
              </View>;
          }
      }
      return (
          <Container>
              <Content>
                  {CatalogueList}
              </Content>
          </Container>
      );
  }
}

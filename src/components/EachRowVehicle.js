import React from "react";
import { Col, Row, Grid } from "react-native-easy-grid";
import { observer, inject } from "mobx-react";
import Fa from "react-native-vector-icons/FontAwesome";
import {Text, Image,TouchableHighlight,Alert,View,TouchableOpacity} from "react-native";
import { Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from "native-base";
import {PUBLIC_DOMAIN} from "../../constants";
class CustomImage extends React.Component{
    constructor(props){
        super(props);
        this.state={
            imageUri:this.props.url,
        };
    }
    render(){
        return(
            <Image
                defaultSource={require("../../assets/noimagefound.jpg")}
                source={{uri:this.state.imageUri}} style={{height:50,width:50,borderRadius:25, width: null,flex:1}}
            />
        );
    }
}
@inject(["ThemeStore"])
@observer
export default class EachRowVehicle extends React.Component {
    render(){
        const ThemeStore=this.props.ThemeStore;
        const navigation=this.props.navigation;
        const data=this.props.data;
        return(
            <TouchableOpacity onPress={()=>this.selectSupplier(data)}>
                <Card  key={data.id}>
                    <CardItem style={{flex:1,flexDirection:"row"}}>
                        <View style={{width:50,height:50,marginRight:10}}>
                            <CustomImage url={data.serviceProviderObj.serviceProviderIconPath} />
                        </View>
                        <View style={{width:250}}>
                            <Text style={{color:ThemeStore.textColor}}>{data.serviceProviderObj.serviceProvider}</Text>
                            <View style={{width:300,flexDirection:"row"}}>
                                <Text style={{color:ThemeStore.textColor,width:125,alignSelf:"left"}}>{data.vehicle_brand}</Text>
                                <Text style={{color:"black",fontWeight: "bold",alignSelf:"right",width:125}}>{data.serviceObj.itemDefinition}</Text>
                            </View>
                            <View style={{width:300,flexDirection:"row"}}>
                                <Text style={{color:ThemeStore.textColor,width:125,alignSelf:"left"}}>{"\n"}Status  </Text>
                                <Text style={{color:"black",fontWeight: "bold",alignSelf:"right",width:125,alignSelf:"right"}}>{"\n"}{data.status}</Text>
                            </View>
                        </View>
                        <View style={{width:70,position:"absolute",right:10}}>
                            <Image
                                defaultSource={require("../../assets/noimagefound.jpg")}
                                source={{uri:data.serviceProviderServiceObj.driverImage}} style={{height:50,width:50,borderRadius:25  }}
                            />
                        </View>
                    </CardItem>
                    <CardItem>
                        <Body>
                        </Body>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        );
    }
}

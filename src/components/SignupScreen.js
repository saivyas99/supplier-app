import React from "react";
import { observer, inject } from "mobx-react";
import {View,Image,TextInput,ScrollView} from "react-native";
import {Button,Text,Item,Input} from "native-base";
import {APP_NAME} from "../../constants";
import PhoneInput from "react-native-phone-input";
import { NativeModules } from "react-native";
@inject(["ThemeStore"])
@observer
export default class SignupScreen extends React.Component{
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:APP_NAME,
  });
  constructor(props){
      super(props);
  }
  state = {
      isValid:false,
      mailid:"",
  };
  componentWillMount() {
  }
  componentDidMount(){
  }
  signup(){
      const UserStore=this.props.UserStore;
  }
  render() {
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <ScrollView>
              <View style={{flex:1,flexDirection:"column",alignItems:"center",justifyContent:"center",padding:20}}>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Text style={{color:"#000"}}>Supplier Signup</Text>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({mailid:text})} placeholder="Brand Name" />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({password:text})} placeholder="Contact Person" />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({password:text})} placeholder="Contact Person" />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({password:text})} placeholder="Contact Number" />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({password:text})} placeholder="e-Maild Id" />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({password:text})} placeholder="Address" />
                      </Item>
                  </View>
                  <Button block style={{backgroundColor:ThemeStore.buttonColors,width:"80%"}}  onPress={() => this.VaidateLogin()} block style={{backgroundColor:this.state.disabledColor,width:"90%",marginLeft:"5%"}}>
                      <Text style={{color:ThemeStore.buttonTextColor}}>Signup</Text>
                  </Button>
              </View>
          </ScrollView>
      );
  }
}

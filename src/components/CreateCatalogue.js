import React from "react";
import { observer, inject } from "mobx-react";
import {View,Image,TextInput,ScrollView} from "react-native";
import {Button,Text,Item,Input,Icon,Picker,Tab, Tabs} from "native-base";
import {APP_NAME} from "../../constants";
import PhoneInput from "react-native-phone-input";
import { NativeModules } from "react-native";
import Icon1 from "react-native-vector-icons/MaterialCommunityIcons";
import CatalogueList from "./CatalogueList.js";
import { ImagePicker } from "expo";
@inject(["ThemeStore"],["SupplierStore"])
@observer
class NewCatalogoue extends React.Component{
  state = {
      product_name:"",
      storage:"",
      price:"",
      capacity:"",
      delivery_time:"",
      product_description:""
  };
  createCatalogue(){
      const SupplierStore=this.props.SupplierStore;
      const navigation=this.props.navigation;
      if(this.state.isValid){
          let post_json ={
              "name":this.state.name,
              "mobileNumber":this.phone.getValue()
          };
          SupplierStore.userLogin(post_json);
          navigation.navigate("OtpScreen");
      }
  }
  onValueChange2(value: string) {
      this.setState({
          storage: value
      });
  }
  _pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          base64: true,
          aspect: [4, 3]
      });
      if (!result.cancelled) {
          this.setState({ image: result.uri });
          const response = await fetch(result.uri);
          const blob = await response.blob();           // Post this blob to server
      };
  }
  render(){
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <ScrollView>
              <View style={{flex:1,flexDirection:"column",paddingLeft:10,paddingRight:10}}>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input style={{width:"80%"}}  onChangeText={(text) => this.setState({mailid:text})} placeholder="  Product Name" />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name="ios-arrow-down-outline" />}
                              style={{ width: "80%" }}
                              placeholder="Storage"
                              placeholderStyle={{ color: "#000"}}
                              placeholderIconColor="#000"
                              selectedValue={this.state.selected2}
                              onValueChange={this.onValueChange2.bind(this)}
                          >
                              <Picker.Item label="Value 1" value="Value 1" />
                              <Picker.Item label="Value 2" value="key1" />
                              <Picker.Item label="Value 3" value="key2" />
                              <Picker.Item label="Value 4" value="key3" />
                              <Picker.Item label="NValue 5" value="key4" />
                          </Picker>
                      </Item>
                  </View>
                  <View  style={{margin:10,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                      <Item style={{width:"50%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input  onChangeText={(text) => this.setState({password:text})} placeholder="  Price" />
                      </Item>
                      <Item style={{width:"50%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input onChangeText={(text) => this.setState({password:text})} placeholder="  Capacity" />
                          <Icon1 style={{color:ThemeStore.buttonColors,fontSize:30}} name='plus-box' />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input style={{width:"80%"}}  onChangeText={(text) => this.setState({password:text})} placeholder="  Delivery Time" />
                          <Icon1 style={{color:ThemeStore.buttonColors,fontSize:30}} name='plus-box' />
                      </Item>
                  </View>
                  <View style={{margin:10,alignItems:"center",justifyContent:"center"}}>
                      <Item style={{width:"100%",borderBottomColor: "black",borderBottomWidth: 1,marginBottom:10,marginTop:10}}>
                          <Input style={{width:"80%"}} onChangeText={(text) => this.setState({password:text})} placeholder="  Product Description" />
                      </Item>
                  </View>
                  <View style={{flexDirection:"row",alignSelf:"center",margin:10}}>
                      <Icon  onPress={() => this._pickImage()} style={{color:ThemeStore.buttonColors,fontSize:50}} name="md-camera" />
                      <Button style={{backgroundColor:ThemeStore.buttonColors,width:"50%",alignSelf:"center"}}  onPress={() => this.VaidateLogin()} block style={{backgroundColor:this.state.disabledColor,width:"90%",marginLeft:"5%"}}>
                          <Text style={{color:ThemeStore.buttonTextColor}}>Submit</Text>
                      </Button>
                  </View>
              </View>
          </ScrollView>
      );
  }
}
@inject(["ThemeStore"],["SupplierStore"])
@observer
export default class CreateCatalogue extends React.Component{
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:"Create Catalogue",
      headerLeft:<Button transparent onPress={()=>navigation.goBack()} style={{width:100}}><Icon name="md-arrow-back"  style={{color:screenProps.iconColor.color,marginLeft:5,fontSize:20,padding:10}} /></Button>,
      headerRight:<View  style={{marginRight:5}} />,
  });
  constructor(props){
      super(props);
  }
  componentWillMount() {
  }
  componentDidMount(){
  }
  render() {
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <Tabs>
              <Tab heading="New  Catalogue">
                  <NewCatalogoue navigation={navigation} />
              </Tab>
              <Tab heading="Catalogue List">
                  <CatalogueList navigation={navigation} />
              </Tab>
          </Tabs>
      );
  }
}

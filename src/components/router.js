import React from "react";
import { StyleSheet, TouchableWithoutFeedback,Text, View ,ScrollView,ActivityIndicator,Image,TouchableHighlight,BackHandler} from "react-native";
import { Container, Header, Content,Button,Card,CardItem,Body,Icon} from "native-base";
import { StackNavigator,DrawerNavigator } from "react-navigation";
import { DrawerItems, DrawerNavigation,TabNavigator} from "react-navigation";
import { observer, inject } from "mobx-react";
import Fa from "react-native-vector-icons/FontAwesome";
import LoginScreen from "./LoginScreen";
import ThemeSettings from "./ThemeSettings";
import Settings from "./Settings";
import ThemeChanging from "./ThemeChanging";
import SplashScreen from "./SplashScreen";
import SignupScreen from "./SignupScreen";
import Accounts from "./Accounts";
import SelectArea from "./SelectArea";
import OrderTrack from "./OrderTrack";
import CreateCatalogue from "./CreateCatalogue";
import UpdateProfile from "./UpdateProfile";
import OrderHistory from "./OrderHistory";
import VehicleDetails from "./VehicleDetails";
import DriverDetails from "./DriverDetails";
import WaterResource from "./WaterResource";
import Support from "./Support";
import {LOGO_IMG,APP_NAME} from "../../constants";
const Stack = StackNavigator({
    SplashScreen:{screen:SplashScreen},
    Support:{screen:Support},
    WaterResource:{screen:WaterResource},
    DriverDetails:{screen:DriverDetails},
    VehicleDetails:{screen:VehicleDetails},
    CreateCatalogue:{screen:CreateCatalogue},
    Accounts:{screen:Accounts},
    OrderHistory:{screen:OrderHistory},
    ThemeSettings:{screen:ThemeSettings},
    Settings:{screen:Settings},
    ThemeChanging:{screen:ThemeChanging},
    LoginScreen:{screen:LoginScreen},
    SignupScreen:{screen:SignupScreen},
    SelectArea:{screen:SelectArea},
    OrderTrack:{screen:OrderTrack},
    UpdateProfile:{screen:UpdateProfile},
},
{
    navigationOptions:({navigation,screenProps})=>({
        title:APP_NAME,
        headerTitleStyle :screenProps.headerTitleStyle,
        headerStyle:screenProps.headerStyle,
        headerLeft:<Button transparent onPress={()=>navigation.navigate("DrawerOpen")} style={{width:100}}><Icon name="menu"  style={{color:screenProps.iconColor.color,marginLeft:5,fontSize:20,padding:10}} /></Button>,
    })
});
@inject(["ThemeStore"])
@observer
export default class DrawerContent extends React.Component {
    componentWillMount(){
        const navigation=this.props.navigation;
        const ThemeStore=this.props.ThemeStore;
    }
    render(){
        const JobStore=this.props.JobStore;
        const ThemeStore=this.props.ThemeStore;
        const navigation =this.props.navigation;
        let imgTag=<Image source={LOGO_IMG} style={{width:150,height:150,backgroundColor:"transparent",borderRadius:75,borderWidth:1,borderColor:ThemeStore.headerTextColor}} />;
        return(
            <Container>
                <Content>
                    <View
                        style={{
                            backgroundColor: ThemeStore.statusBarBackgroundColor,
                            height: 150,
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        {imgTag}
                    </View>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("OrderHistory")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}} >
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Orders</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("OrderTrack")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}} >
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Track Order</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("Accounts")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}}  >
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Accounts</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("CreateCatalogue")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}}>
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Catalogue</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("VehicleDetails")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}}>
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Vehicle Details</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("WaterResource")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}} >
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Water Resource</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("DriverDetails")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}} >
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Driver Details</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=>navigation.navigate("Support")}>
                        <Card style={{backgroundColor:ThemeStore.buttonColors}} >
                            <CardItem style={{backgroundColor:ThemeStore.buttonColors}}>
                                <Fa name="heart" style={{fontSize:15,color:ThemeStore.buttonTextColor,width:20,marginRight:10}}/>
                                <Body>
                                    <Text style={{color:ThemeStore.buttonTextColor}}>Support</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableWithoutFeedback>
                </Content>
            </Container>
        );
    }
}
const Drawer = DrawerNavigator(
    {
        Home: {screen: Stack},
    },
    {
        contentComponent: ({ navigation }) => (
            <DrawerContent  navigation={navigation} />
        ),
    }
);
module.exports = Drawer;
